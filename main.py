from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from datetime import timedelta
import datetime
from sqlalchemy import Column, Integer, DateTime, String, ForeignKey, Boolean
from sqlalchemy import or_, and_, desc
from sqlalchemy.orm import relationship
import bcrypt
import json
from functools import wraps
import jwt


# Init app
app = Flask(__name__, template_folder='frontend')
# Database

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://admin:Rocky4692' \
    '@chatappr.cslybck7djf3.ap-south-1.rds.amazonaws.com/chat_app'
# app.config['SQLALCHEMY_DATABASE_URI'] =
# 'mysql+pymysql://root:@localhost/chat_app'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'skdf23bk43i9d33r'
# Init db
db = SQLAlchemy(app)
# Init ma
ma = Marshmallow(app)

# User Class/Model defination


class User(db.Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    username = Column(String(200))
    password = Column(String(200))
    email = Column(String(512))
    chat = relationship("Chat",
                        primaryjoin="User.id==Chat.user_id1")

    created_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, name, username, email, password):
        self.name = name
        self.username = username
        self.email = email
        self.password = password


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'username', 'email', 'password')

# Chat class defination


class Chat(db.Model):
    __tablename__ = 'chat'
    id = Column(Integer, primary_key=True)
    message = Column(String(2048))
    type = Column(String(256), default="P")
    user_id1 = Column(Integer, ForeignKey('user.id'))
    user_id2 = Column(Integer, ForeignKey('user.id'), nullable=True)
    group_id = Column(Integer, ForeignKey('group.id'), nullable=True)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, message, user_id1, user_id2, group_id):
        self.message = message
        self.user_id1 = user_id1
        self.user_id2 = user_id2
        self.group_id = group_id


class ChatSchema(ma.Schema):
    class Meta:
        fields = ('id', 'message', 'user_id1',
                  'user_id2', 'group_id', 'created_at')

# Group Class defination


class Group(db.Model):
    __tablename__ = 'group'
    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    description = Column(String(512))
    chats = relationship("Chat")
    members = relationship("Member")
    created_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, name, description):
        self.name = name
        self.description = description


class GroupSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'description')

# Member Class defination


class Member(db.Model):
    __tablename__ = 'members'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    group_id = Column(Integer, ForeignKey('group.id'))
    is_admin = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, user_id, group_id, is_admin):
        self.group_id = group_id
        self.user_id = user_id
        self.is_admin = is_admin


class MemberSchema(ma.Schema):
    class Meta:
        fields = ('id', 'group_id', 'is_admin')


# User schema Init
user_schema = UserSchema()
users_schema = UserSchema(many=True)

# Chat schema Init
chat_schema = ChatSchema()
chats_schema = ChatSchema(many=True)

# Group schema Init
group_schema = GroupSchema()
groups_schema = GroupSchema(many=True)

# Member schema Init
member_schema = MemberSchema()
members_schema = MemberSchema(many=True)

# function for verifying password hash


def verify_password(user, password):
    return bcrypt.checkpw(password.encode('utf-8'),
                          user["password"].encode('utf-8'))

# decorator for verifying the JWT


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        # jwt is passed in the request header
        if 'Authorization' in request.headers:
            token = request.headers['Authorization'].split(" ")[1]
        # return 401 if token is not passed
        if not token:
            return jsonify({'error': True, 'message': 'Token is missing !!'}),\
                401

        try:
            # decoding the payload to fetch the stored details
            data = jwt.decode(
                token, app.config['SECRET_KEY'], algorithms="HS256")
            print(data)
            current_user = json.loads(user_schema.jsonify(
                User.query.filter_by(id=data['user_id']).first()).data)
        except Exception as err:
            print(err)
            return jsonify({
                'error': True,
                'message': 'Token is invalid !!'
            }), 401
        # returns the current logged in users contex to the routes
        return f(current_user, *args, **kwargs)

    return decorated


# @app.route('/user/test', methods=['POST'])
# @token_required
# def test(f):
#     return {"error": False, "data": f}

# api_link = 'http://127.0.0.1:5000'
api_link = 'http://52.66.246.52'


@app.route('/login', methods=['GET'])
def login_page():
    return render_template('login.html', api=api_link)


@app.route('/signup', methods=['GET'])
def signup_page():
    return render_template('signup.html', api=api_link)


@app.route('/', methods=['GET'])
def home_page():
    return render_template('index.html', api=api_link)


@app.route('/group', methods=['GET'])
def group_page():
    return render_template('group.html', api=api_link)


@app.route('/chat', methods=['GET'])
def chat_page():
    return render_template('chat.html', api=api_link)

# Singup user route


@app.route('/user/signup', methods=['POST'])
def signup():
    try:
        name = request.json['name']
        username = request.json['username']
        email = request.json['email']
        check_user_exist = json.loads(user_schema.jsonify(
            User.query.filter_by(username=username, email=email).first()).data)
        if(check_user_exist != {}):
            raise Exception('User already Exists')
        password = bcrypt.hashpw(
            request.json['password'].encode('utf-8'), bcrypt.gensalt())
        new_user = User(name, username, email, password)
        db.session.add(new_user)
        db.session.commit()
        db.session.refresh(new_user)
        data = json.loads(user_schema.jsonify(new_user).data)
        del data['password']
        # token = jwt.encode({
        #     'user_id': new_user['id'],
        #     'exp': datetime.datetime.utcnow() + timedelta(minutes=30)
        # }, app.config['SECRET_KEY'], algorithm="HS256")
        return {"error": False, "message": "User Signup Success", "user": data}
    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}


# User login route
@app.route('/user/login', methods=['POST'])
def login():
    try:
        username = request.json['username']
        password = request.json['password']
        new_user = json.loads(user_schema.jsonify(
            User.query.filter_by(username=username).first()).data)
        if new_user == {}:
            raise Exception('User Not found')
        elif verify_password(new_user, password) is False:
            raise Exception('Password Does Not Match')
        del new_user['password']
        token = jwt.encode({
            'user_id': new_user['id'],
            'exp': datetime.datetime.utcnow() + timedelta(minutes=30)
        }, app.config['SECRET_KEY'], algorithm="HS256")

        ReturnData = {
            "error": False,
            "message": "Login Success",
            "user": new_user,
            "token": token}
        return ReturnData
    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}


# User Personal Chat route
@app.route('/user/chat', methods=['POST'])
@token_required
def chat(user):
    try:
        user_id2 = request.json['username']
        message = request.json['message']
        new_user2 = json.loads(user_schema.jsonify(
            User.query.filter_by(username=user_id2).first()).data)
        if(new_user2 == {}):
            raise Exception('User Not found')
        new_message = Chat(message, user['id'], new_user2['id'], None)
        db.session.add(new_message)
        db.session.commit()
        chats = \
            json.loads(chats_schema.jsonify
                       (Chat.query.filter(or_(and_(Chat.user_id1 == user['id'],
                        Chat.user_id2 == new_user2['id']),
                        and_(Chat.user_id1 == new_user2['id'],
                        Chat.user_id2 == user['id']))).order_by
                        (desc('created_at'))).data)
        return {"error": False, "message": "chat found", "chats": chats}

    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}


@app.route('/user/chat/get', methods=['POST'])
@token_required
def get_chat(user):
    try:
        to_username = request.json['username']
        new_user = json.loads(user_schema.jsonify(
            User.query.filter_by(username=to_username).first()).data)
        chats = \
            json.loads(chats_schema.jsonify
                       (Chat.query.filter(or_(and_(Chat.user_id1 == user['id'],
                        Chat.user_id2 == new_user['id']),
                        and_(Chat.user_id1 == new_user['id'],
                             Chat.user_id2 == user['id']))).order_by
                        (desc('created_at'))).data)
        user_details = {}
        new_user = json.loads(user_schema.jsonify(
            User.query.filter_by(id=user['id']).first()).data)
        user_details[user['id']] = user['username']
        print(user_details)
        for usertmp in chats:
            new_user = json.loads(user_schema.jsonify(
                User.query.filter_by(id=usertmp['user_id2']).first()).data)
            user_details[new_user['id']] = new_user['username']
        print(chats)
        return {"error": False,
                "message": "chat found",
                "chats": chats,
                "user_details": user_details}
    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}

    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}

# Create Group route


@ app.route('/group/create', methods=['POST'])
@ token_required
def creat_group(user):
    try:
        group_name = request.json['group_name']
        group_description = request.json['group_desc']
        new_group = Group(group_name, group_description)
        db.session.add(new_group)
        db.session.commit()
        db.session.refresh(new_group)
        group_data = json.loads(group_schema.jsonify(new_group).data)
        new_member = Member(user['id'], group_data['id'], True)
        db.session.add(new_member)
        db.session.commit()
        data = json.loads(member_schema.jsonify(new_member).data)
        return {"error": False,
                "message": "Group Created Successfully",
                "group": group_data,
                "member_details": data}
    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}


@app.route('/user/chat/recent', methods=['GET'])
@ token_required
def get_user_recent_chats(user):
    try:
        queryy = f"SELECT user.id AS id, \
        user.username AS username FROM user INNER JOIN chat ON chat.user_id1 =\
        user.id OR chat.user_id2 = user.id WHERE chat.user_id1 ={user['id']} \
            OR chat.user_id2 = \
                 {user['id']} GROUP BY user.id ORDER BY chat.created_at"
        print(queryy)
        user_chats = db.engine.execute(queryy)
        usernames = []
        for chat in user_chats:
            if chat[0] != user['id']:
                usernames.append(
                    {"id": chat[0], "username": chat[1], "type": "USER"})

        queryy2 = f"SELECT * FROM `group`INNER JOIN members \
             ON members.group_id = group.id WHERE members.user_id \
                 = {user['id']} GROUP BY members.user_id \
                     ORDER BY group.created_at"
        group_chats = db.engine.execute(queryy2)
        for group in group_chats:
            usernames.append(
                {"group_id": group[0],
                 "group_name": group[1],
                 "group_desc": group[2],
                 "type": "GROUP"})
        print(usernames)
        return {"error": False, "message": "chat found", "chats": usernames}
    except Exception as err:
        print(err)
        return {"error": True, "message": str(err)}

    # Get All users route


@ app.route('/user/getAllUsers', methods=['GET'])
def getAllUsers():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    print(result)
    return jsonify(result)


# Run Server
if __name__ == '__main__':
    app.run(debug=True)
